from os import listdir
from os.path import isfile, join
import numpy as np
import matplotlib.pyplot as plt
from natsort import natsorted, ns

images_axial = []
images_coronal = []
images_sagittal = []
index_axial = 180
index_coronal = 180
index_sagittal = 180
VMIN = -255
VMAX = 255


def get_images(path='data'):
    axials, coronals, sagittals = [], [], []
    onlyfiles = natsorted(
        [join(path, f) for f in listdir(path) if isfile(join(path, f))], alg=ns.IGNORECASE)
    cont_axial = 0
    for filename in onlyfiles:
        f = open(filename, 'rb')
        img_str = f.read()
        img_arr = np.fromstring(img_str, dtype='int16')
        im1 = np.reshape(img_arr, (512, 512))
        axials.append(im1)
        cont_axial += 1
    for x in range(512):
        c, s = [], []
        cont_axial = 1
        for image in axials:
            c.append(image[x])
            image2 = []
            for i in image:
                image2.append(i[x])
            s.append(image2)
            print(f'{x}/512 - {cont_axial}/{len(axials)}')
            cont_axial += 1
        coronals.append(np.array(c))
        sagittals.append(np.array(s))
    print('Pronto!')
    return axials, sagittals, coronals


def onclick(event):
    plt.cla()
    index_coronal = int(event.ydata)
    index_sagittal = int(event.xdata)
    coronal = images_coronal[index_coronal]
    sagittal = images_sagittal[index_sagittal]
    axs[0].imshow(coronal, cmap='gray', vmin=VMIN, vmax=VMAX)
    axs[2].imshow(sagittal, cmap='gray', vmin=VMIN, vmax=VMAX)
    axs[0].texts = []
    axs[0].text(0, -5, f'Coronal - Y: {int(index_coronal)}')
    axs[2].texts = []
    axs[2].text(0, -5, f'Sagittal - X: {int(index_sagittal)}')
    plt.gcf().canvas.draw_idle()


def onscroll(event):
    global index_axial
    if event.button == 'up' and index_axial > 0:
        index_axial -= 1
    elif event.button == 'down' and index_axial < 360:
        index_axial += 1
    axs[1].imshow(images_axial[index_axial], cmap='gray', vmin=VMIN, vmax=VMAX)
    axs[1].texts = []
    axs[1].text(0, -5, f'Axial - Image: {index_axial}')
    plt.gcf().canvas.draw_idle()


if __name__ == '__main__':
    images_axial, images_sagittal, images_coronal = get_images()
    fig, axs = plt.subplots(1, 3, sharex=False, sharey=False)
    fig.canvas.mpl_connect('button_press_event', onclick)
    fig.canvas.mpl_connect('scroll_event', onscroll)
    axs[0].texts = []
    axs[0].text(0, -5, f'Coronal - Y: {int(index_coronal)}')
    axs[0].imshow(images_coronal[index_coronal], cmap='gray', vmin=VMIN, vmax=VMAX)
    axs[1].imshow(images_axial[index_axial], cmap='gray', vmin=VMIN, vmax=VMAX)
    axs[1].text(0, -5, f'Axial - Image: {index_axial}')
    axs[2].texts = []
    axs[2].text(0, -5, f'Sagittal - X: {int(index_sagittal)}')
    axs[2].imshow(images_sagittal[index_sagittal], cmap='gray', vmin=VMIN, vmax=VMAX)
    plt.show()
