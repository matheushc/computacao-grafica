import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

with open('points') as file:
    pts = file.readlines()

polygons = []
for pt in pts:
    p = eval(pt)
    polygons.append(p)

# polygons = [[[0, 0, 0], [200, 0, 0], [100, 200, 0]]]
plt.figure()
custom = plt.subplot(121, projection='3d')

for t in polygons:
    x1 = np.array(t[0])
    y1 = np.array(t[1])
    z1 = np.array(t[2])
    custom.scatter(x1, y1, z1)

    # 1. create vertices from points
    verts = [list(zip(x1, y1, z1))]
    # 2. create 3d polygons and specify parameters
    srf = Poly3DCollection(verts, alpha=.25, facecolor='#800000')
    # 3. add polygon to the figure (current axes)
    plt.gca().add_collection3d(srf)

custom.set_xlabel('X')
custom.set_ylabel('Y')
custom.set_zlabel('Z')
plt.show()
