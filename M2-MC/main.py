from os import listdir
from os.path import isfile, join
import numpy as np
import matplotlib.pyplot as plt
from natsort import natsorted, ns
from skimage.transform import resize
from skimage import img_as_int

edges = [
         (1, 2, 2),
         (2, 2, 1),
         (1, 2, 0),
         (0, 2, 1),
         (1, 0, 2),
         (2, 0, 1),
         (1, 0, 0),
         (0, 0, 1),
         (0, 1, 2),
         (2, 1, 2),
         (2, 1, 0),
         (0, 1, 0)
]
vertex = [
          (0, 2, 2),
          (2, 2, 2),
          (2, 2, 0),
          (0, 2, 0),
          (0, 0, 2),
          (2, 0, 2),
          (2, 0, 0),
          (0, 0, 0)
]


def get_images(path='data'):
    axials = []
    cmap = plt.cm.bone
    norm = plt.Normalize(vmin=244, vmax=245)

    onlyfiles = natsorted(
        [join(path, f) for f in listdir(path) if isfile(join(path, f))], alg=ns.IGNORECASE)
    cont_axial = 0
    onlyfiles = onlyfiles[:20]
    for filename in onlyfiles:
        f = open(filename, 'rb')
        img_str = f.read()
        img_arr = np.fromstring(img_str, dtype='int16')
        image = np.reshape(img_arr, (512, 512))
        resized = img_as_int(resize(image, (200, 200)))
        print(f'{cont_axial}/{len(onlyfiles)-1}')
        axials.append(cmap(norm(resized)))
        cont_axial += 1
    print('Pronto!')
    return axials


def get_cubes(images):
    cube = 3  # 3x3x3
    cubes = []
    count = 0
    for index_image in range(1, len(images)-1, 6):  # imagem
        imgs = [images[index_image-1], images[index_image], images[index_image+1]]

        for index_line in range(1, len(imgs[0])-1, 6):
            lines = [imgs[0][index_line-1], imgs[0][index_line], imgs[0][index_line+1]]

            for index_column in range(1, len(lines[0])-1, 6):
                columns = [lines[0][index_column-1], lines[0][index_column], lines[0][index_column+1]]

                new_cube = []
                for z in imgs:
                    new_z = []
                    for y in lines:
                        new_y = []
                        for x in columns:
                            # if x == 1:
                            #     print(count)
                            new_y.append(x)
                        new_z.append(np.array(new_y))
                    new_cube.append(np.array(new_z))
                cubes.append(np.array(new_cube))
                count += 1
    return np.array(cubes)


def get_points(cube):
    # print(cube)
    # print('points')
    points = []
    for z, image in enumerate(cube):
        for y, line in enumerate(image):
            for x, colunm in enumerate(line):
                if np.array_equal(colunm, np.array([1, 1, 1, 1])):
                    print((x, y, z))
                    points.append((x, y, z))
    return points


def get_all_points(cubes):
    all_points = []
    for cube in cubes:
        count_z = 0
        points = []
        for z, image in enumerate(cube):
            count_y = 0
            for y, line in enumerate(image):
                count_x = 0
                for x, colunm in enumerate(line):
                    if np.array_equal(colunm, np.array([1, 1, 1, 1])):
                        points.append((z*count_z*7, +y*count_y*7, +x*count_x*7))
                    count_x += 1
                count_y += 1
            count_z += 1
        all_points.append(points)
    return all_points


def main():  # {'[1. 1. 1. 1.]', '[0. 0. 0. 1.]'} # np.array([0, 0, 0, 1])  np.array([1, 1, 1, 1])
    images = get_images()
    # new_images = transform_images(images)

    cubes = get_cubes(images)
    print(len(cubes))

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    all_pts = get_all_points(cubes)

    for count, pts in enumerate(all_pts):
        for pt in pts:
            print(f'Plot {count}/{len(all_pts)}')
            print(pt)
            ax.scatter(pt[0], pt[1], pt[2], c='black')
    plt.show()


if __name__ == '__main__':
    main()
